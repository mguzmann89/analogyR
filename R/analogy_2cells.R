#' analogy_2cells
#'
#' Build the proportion between froms `s` to `t`.
#' The proprotion is the full alignment + numbers for corresponding material.
#'
#' @param cell1 Cell 1
#' @param cell2 Cell 2
#' @param .metathesis Maximum window to search for metathesis.
#' If 0 do not search for metathesis.
#'
#' @keywords proportion, alignments
#'
#' @export
#' 
#' @examples
#' make_proportion("como", "comes")
analogy_2cells <- function(cell1, cell2, .metathesis = TRUE) {
    ## build analogies
    ans <- analogy_build(cell1, cell2, .metathesis = .metathesis)
    ## get unique analogies
    ans_u <- unique(unlist(ans))
    ## find matches of the analogies in the dataset
    message("Applying analogies... ")
    matches <- analogy_fits(cell1, cell2, ans_u, .nest = "str")
    if (!all(sapply(matches, any))) {
        bad_i <- which(!(sapply(matches, any)))
        warning("There is an issue with the analogies. Please check: "
              , paste(bad_i, collapse = ", "))
    }
    ## find coverage of each unique analogy
    an_coverage <- as.data.frame(do.call(rbind, matches))
    colnames(an_coverage) <- ans_u
    an_coverage <- colSums(an_coverage, na.rm = TRUE)
    ## pick best analogies
    message("Picking best analogies...")
    sapply(matches, function(mtch) {
        analogy_pick(an_coverage[mtch])
    })
}
