#' find_paths
#'
#' Find all paths for converting `s` into `t`
#' @param s Source string
#' @param t Target string
#' @param .split For splitting the strings. Defaults to "" for separating by characters.
#' @param .sep For separating the paths. Should not appear in the strings.
#' @keywords weights, probabilities
#' @return A vector of transformation paths applied to a Levenshtein alignment from `s` to `t`.
#' A path can contain 'l' (left), 'd' (diagonal) or 'u' (up)
#' @export
#' @examples
#' find_paths("poder", "puedo") # -> c("dluudllu", "dlludluu", "dlllduuu")
find_paths <- function(t, s, .split = "") {
    sub_sort <- function(path) {
        unlist(lapply(unlist(strsplit(path, "(?=d)", perl = TRUE)), str_split, "")
             , F, F) %>% lapply(sort) %>% unlist() %>%
            paste0(collapse = "")
    }
    als <- get_alignments_cpp(get_backtrace2_cpp(str_split0(t, .split)
                                              , str_split0(s, .split)))
    als_u <- unique(str_split0(paste0(als, collapse = ""), "#"))
    als_u <- unique(sapply(als_u[als_u!=""], sub_sort))
    als_u <- als_u[which_maxs(str_count(als_u, "d"))]
    als_u[which_mins(str_length(als_u))]
}
