#' Softmax function
#'
#' Computes probabilities from weights.
#' @param x Vector of weights.
#' @keywords weights, softmax, probabilities.
#' @return A vector of probabilities based on the softmax function on `x`.
#' @export
#' @examples
#' softmax(c(1, 2, 3, 4))
softmax <- function (x) {
    logsumexp <- function (x) {
        y = max(x)
        y + log(sum(exp(x - y)))
    }
    exp(x - logsumexp(x))
}
