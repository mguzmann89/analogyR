#include <Rcpp.h>

using namespace Rcpp;

// [[Rcpp::plugins(cpp11)]]

//' align_cpp
//' 
//' Return alternation from string and alignment
//'
//' @param str1 string as vector of characters
//' @param str2 string as vector of characters
//' @param al alignment as vector of characters
//' @export
//' @return A vector with the alternation (unsimplified).
// [[Rcpp::export]]
std::vector<std::vector<std::string> > align_cpp(std::vector<std::string> str1,
						 std::vector<std::string> str2,
						std::vector<std::string> al){
  std::vector<std::vector<std::string> >
    res (2, std::vector<std::string>(al.size()));
  int j1 = 0;
  int j2 = 0;
  for (int i = 0; i < al.size(); i++) {
    if (al[i] == "d") {
      res[0][i] = str1[j1];
      res[1][i] = str2[j2];
      j1 ++;
      j2 ++;
    } else if (al[i] == "u") {
      res[0][i] = str1[j1];
      res[1][i] = "-";
      j1 ++;
    } else {
      res[0][i] = "-";
      res[1][i] = str2[j2];
      j2 ++;
    }
  }
  return res;
}
