#ifndef ALIGN_H
#define ALIGN_H

#include <Rcpp.h>

// [[Rcpp::plugins(cpp11)]]

using namespace Rcpp;

std::vector<std::vector<std::string>> align_cpp(std::vector<std::string> str1,
						std::vector<std::string> str2,
						std::vector<std::string> al);
#endif
