#include <RcppParallel.h>
#include <Rcpp.h>
#include "analogy-utils.h"

using namespace Rcpp;
using namespace RcppParallel;

// [[Rcpp::depends(RcppParallel)]]
// [[Rcpp::plugins(cpp11)]]

//' analogy_apply_cpp
//' 
//' Apply analogy to string
//'
//' @param str String to apply to
//' @param analogy Full analogy to apply
//' 
//' @return A string 
//'
//' @export
//' 
//' @examples
//' 
//' analogy_apply_cpp(c("a", "b", "a"), c("<X1,1>", "b", "a", "⇋", "<X1,1>))      # => "a"
//' analogy_apply_cpp(c("a", "d", "b", "a"), c("<X1,1>", "b", "a", "⇋", "<X1,1>)) # => "ad"
//' 
// [[Rcpp::export]]
std::string analogy_apply_cpp(const std::vector<std::string> str,
			      const std::vector<std::string> analogy) {

  if (!is_analogy_cpp(analogy)) {
    Rcout << "Not a valid analogy.";
    std::string res_str = string_flatten_cpp(str);
    return res_str;
  }
  
  std::vector<std::string> analogy_left;
  analogy_left.reserve(analogy.size());
  std::vector<std::string> analogy_right;
  analogy_right.reserve(analogy.size());

  bool right = false;
  for (int i = 0; i < analogy.size(); i++) {
    if (analogy[i] == "⇋")
      right = true;
    else if (right){
      analogy_right.push_back(analogy[i]);
    } else {
      analogy_left.push_back(analogy[i]);
    }
  }

  // if the string fewer elements than the left hand side,
  // the analogy cannot match
  if (str.size() < analogy_left.size()) {
    std::string res_str = string_flatten_cpp(str);
    return res_str;
  }
  if (str == analogy_left) {
    std::string res_str = string_flatten_cpp(analogy_right);
    return res_str;
  }
  
  // return variable
  std::vector<std::string> res;
  res.reserve(str.size() * 5);

  // first find all reps for variables
  // and the total number

  int total_match = 0;
  int var_lengths = 0;
  std::map<int, int> var_reps;
  std::map<int, std::vector<std::string>> var_matches;

  // calculate the max number of matching characters for the free variable
  for (int i = 0; i < analogy_left.size(); i++) {

    if (is_var_cpp(analogy_left[i])) {
      int rep = find_var_rep_cpp(analogy_left[i]);
      int id = find_var_id_cpp(analogy_left[i]);
      var_reps[id] = rep;
      total_match = total_match + rep;
    } else {
      var_lengths++;
    }
    
  }

  // if it can't be matched because not enough items
  if ((total_match + var_lengths) >= str.size()) {
    std::string res_str = string_flatten_cpp(str);
    return res_str;    
  }
  
  int zero_match = str.size() - total_match - var_lengths;

  // iterate over items in analogy_left and do the matching
  int k = 0;
  for (int i = 0; i < analogy_left.size(); i++ ) {
    if (k >= str.size()) {
      std::string res_str = string_flatten_cpp(str);
      return res_str;
    }
    
    if (is_var_cpp(analogy_left[i])) {
      int rep = find_var_rep_cpp(analogy_left[i]);
      int id = find_var_id_cpp(analogy_left[i]);
      std::vector<std::string> match;
      match.reserve(rep*2);
      
      if (rep == 0) rep = zero_match;
      
      for (int j = 0; j < rep; j++ ) {
	match.push_back(str[k]);
	k++;
      }

      // if key exists but match is not consistent
      // return str: <X1,2> <X1,2> ~ capr
      // X1 should match ca and pr but these are different
      if (var_matches.find(id) == var_matches.end() ) {
	var_matches[id] = match;
      } else {
	if (!(match == var_matches[id])) {
	  return string_flatten_cpp(str);
	}
      }

    } else {
      if (analogy_left[i] != str[k]) {
	std::string res_str = string_flatten_cpp(str);
	return res_str;
      }
      k++;
    }
  }

  // now substitute variables for their matched values
  for (int i = 0; i < analogy_right.size(); i++) {
    if (is_var_cpp(analogy_right[i])) {
      int id = find_var_id_cpp(analogy_right[i]);
      auto match = var_matches[id];
      res.insert(res.end(), match.begin(), match.end());
    } else {
      res.push_back(analogy_right[i]);
    }
  }

  std::string res_str = string_flatten_cpp(res);
  return res_str;
}

//' analogy_apply_parallel_cpp
//' 
//' Parallel version of analogy_apply_cpp
//'
//' @param str List of strings to apply to
//' @param analogy List of analogies to apply
//' @param nest Indicates whether to do paired matching, or to nest along the analogies ("an")
//' or along the strings ("str")
//' If "paired" (the default), both `str` and `analogy` must have the same length.
//' 
//' @return A list of result strings
//'
//' @export
//' 
//' @examples
//' 
//' analogy_apply_cpp(list(c("a", "b", "a"), c("a", "d", "b", "a"))
//'                      , c("<X1,1>", "b", "a", "⇋", "<X1,1>")
//'                      , nest = "an")
//' 
//' 
// [[Rcpp::export]]
std::vector<std::vector<std::string>>
analogy_apply_parallel_cpp(const std::vector<std::vector<std::string>> str,
			   const std::vector<std::vector<std::string>> analogy,
			   const std::string nest = "paired") {
  
  // allocate the matrix we will return
  std::vector<std::vector<std::string>> res;
  
  if (nest == "paired") {
    int reps = str.size();
    res.resize(reps);

    tbb::parallel_for( 0, reps, 1, [&res, &str, &analogy](int i){

      std::string an_res = analogy_apply_cpp(str[i], analogy[i]);
      res[i] = {an_res};

    });
  } else if (nest == "str") {
    int reps = str.size();
    res.resize(reps);

    tbb::parallel_for( 0, reps, 1, [&res, &str, &analogy](int i){

      std::vector<std::string> an_res (analogy.size());
      
      for (int j = 0; j < analogy.size(); j++) {
	an_res[j] = analogy_apply_cpp(str[i], analogy[j]);
      }
      
      res[i] = an_res;
    });
  } else if (nest == "an") {
    int reps = analogy.size();
    res.resize(reps);

    tbb::parallel_for( 0, reps, 1, [&res, &str, &analogy](int i){

      std::vector<std::string> an_res (str.size());
      
      for (int j = 0; j < str.size(); j++) {
	an_res[j] = analogy_apply_cpp(str[j], analogy[i]);
      }
      
      res[i] = an_res;

    });
  } else {
    stop("`nest` has to be either 'paired', 'an' or 'str'.");
  }

  return res;
}
