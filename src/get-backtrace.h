#ifndef BACKTRACE_H
#define BACKTRACE_H

#include <Rcpp.h>

// [[Rcpp::plugins(cpp11)]]

using namespace Rcpp;

std::vector<std::vector<std::string>>
get_backtrace_cpp(const std::vector<std::string> str1,
		  const std::vector<std::string> str2);

std::vector<std::vector<std::string>>
get_backtrace2_cpp(const std::vector<std::string> str1,
		   const std::vector<std::string> str2);

#endif
