#include <Rcpp.h>
#include <RcppParallel.h>
#include "lv-dist.h"
#include "utils.h"
#include "align.h"
#include "analogy-utils.h"
#include "get-alignments.h"
#include "find-paths.h"

using namespace RcppParallel;
using namespace Rcpp;

// [[Rcpp::depends(RcppParallel)]]
// [[Rcpp::plugins(cpp11)]]

std::function< bool (const std::string & , const std::string &) >
comparator = [](const std::string & left, const std::string & right){
  if (is_var_cpp(left) | is_var_cpp(right) | (left == "") | (right == "") |
      (left != right))
    return false;
  else
    return true;
 };

// [[Rcpp::export]]
std::vector<std::vector<std::string>>
find_metathesis_cpp(const std::vector<std::vector<std::string>> analogy,
		    int meta_n) {
  int max_size = analogy[0].size();
  std::vector<std::vector<std::string>> res = analogy;

  // find final variable id
  int max_var = 1;
  for (int i = 0; i < max_size; i++) {
    if(is_var_cpp(analogy[0][i])) {
      int var_id = find_var_id_cpp(analogy[0][i]);
      if (var_id > max_var)
	max_var = var_id;
    }
  }

  // metathesis of length 1
  std::vector<int> found_i;
  found_i.reserve(max_size);
  bool found = false;

  for (int meta_i = meta_n; meta_i > 0; meta_i --) {
    for (int j = 0; j < (max_size - (meta_i*2)); j ++ ) {
      int max_size_2 = max_size - j - 1;

      // do the first analogy
      if (!is_var_cpp(analogy[0][j]))
	found = std::equal(analogy[0].begin() + j,
			   analogy[0].begin() + j + meta_i,
			   analogy[1].begin() + j + meta_i + 1,
			   comparator);
      if (found) {
	found = false;
	max_var++;
	res[0][j] =
	  "<X" + std::to_string(max_var) + "," + std::to_string(meta_i) + ">";
	res[1][j + meta_i + 1] =
	  "<X" + std::to_string(max_var) + "," + std::to_string(meta_i) + ">";
	for (int k = 1; k < meta_i; k++ ) {
	  res[0][k + j] = "";
	  res[1][k + j + meta_i + 1] = "";
	}
      } else {
	// other side
	if (!is_var_cpp(analogy[1][j]))
	  found = std::equal(analogy[1].begin() + j,
			     analogy[1].begin() + j + meta_i,
			     analogy[0].begin() + j + meta_i + 1,
			     comparator);
	if (found) {
	  found = false;
	  max_var++;
	  res[1][j] =
	    "<X" + std::to_string(max_var) + "," + std::to_string(meta_i) + ">";
	  res[0][j + meta_i + 1] =
	    "<X" + std::to_string(max_var) + "," + std::to_string(meta_i) + ">";
	  for (int k = 1; k < meta_i; k++ ) {
	    res[1][k + j] = "";
	    res[0][k + j + meta_i + 1] = "";
	  }
	}
      }
    }
  }
  return res;
}

//' analogy_build_cpp
//' 
//' Returns all proportional analogies between `str1` and `str2`.
//'
//' @param str1 Reference string (as a vector of characters)
//' @param str2 Target string (as a vector of characters)
//' @param metathesis Maximum number of characters which might be involved in a metathesis alternation. If set to 0, no metathesis is considered.
//' 
//' @return A list of analogy pairs.
//' 
//' @examples
//' analogy_build_cpp(c("a", "r", "b", "o")
//'                 , c("a", "r", "b", "a")
//'                 , 0)
// [[Rcpp::export]]
std::vector<std::string>
analogy_build_cpp(const std::vector<std::string> str1,
		  const std::vector<std::string> str2,
		  int metathesis = 5) {

  std::vector<std::vector<std::string>> paths = find_paths_cpp(str1, str2);
  std::vector<std::string> all_analogies;
  all_analogies.reserve(paths.size()*5);

  for (int path_i = 0; path_i < paths.size(); path_i ++ ) {

    std::vector<std::vector<std::string> > alignement = align_cpp(str1, str2, paths[path_i]);
    // an alignement is a vector of 2 vectors of strings
    std::vector<std::vector<std::string> > proportion
      (2, std::vector<std::string>(alignement[0].size(), "-"));

    // first replace identical items with variables with ids
    
    int j = 1;
    bool S = false;
    for (int i = 0; i < alignement[0].size(); i++) {
      if ((alignement[0][i] == alignement[1][i])) { // only change if not already mutated
	proportion[0][i] = "<X" + std::to_string(j);
	proportion[1][i] = "<X" + std::to_string(j);
	S = true;
      } else {
	if (proportion[0][i] == "-") proportion[0][i] = alignement[0][i];
	if (proportion[1][i] == "-") proportion[1][i] = alignement[1][i];
	if(S) {
	  j++;
	  S = false;
	}
      } 
    }

    // next, we replace duplicated variables with their number of repetitions
    
    std::unordered_set<std::string> stack0; // stack for s
    std::unordered_set<std::string> stack1; // stack for t
    std::map<std::string, int> var_counts;

    // the actual analogy for the path in question
    
    std::vector<std::vector<std::string>>
      res (2,std::vector<std::string> (proportion[0].size()));

    // count occurence of each variable
    for (int i = 0; i < proportion[0].size(); i++) {
      if (is_var_cpp(proportion[0][i]))
	var_counts[proportion[0][i]]++;
    }

    auto max_val_vars =
      std::max_element(var_counts.begin(), var_counts.end(),
		       [](const std::pair<std::string, int>& p1,
			  const std::pair<std::string, int>& p2) {
			 return p1.second < p2.second; });

    // varialbes for each time
    std::string item_0 = "";
    std::string item_1 = "";
    int item_0_frq = 0;
    int item_1_frq = 0;

    // iterate over the proportion and replace identities with
    // variables with their matching repetitions
    // we iterate over the proportion and check whether each item is a variable
    // if it is a variable and we have not seen it, we insert it into the proportion
    for (int i = 0; i < proportion[0].size(); i++ ) {
      item_0 = proportion[0][i];
      item_1 = proportion[1][i];

      item_0_frq = var_counts[item_0];
      item_1_frq = var_counts[item_1];

      // if the variable has the highest frequency we set it to 0
      // to indicate multiple matches
      if (item_0 == max_val_vars->first)
	item_0_frq = 0;
      if (item_1 == max_val_vars->first)
	item_1_frq = 0;

      // if is variable and not seen yet
      if (is_var_cpp(item_0) & (stack0.find(item_0) == stack0.end())) {
	res[0][i] = item_0 + "," + std::to_string(item_0_frq) + ">";
	stack0.insert(item_0); // now we've seen it
      } else if ((item_0 != "-") & !is_var_cpp(item_0)){ // otherwise copy the item
	res[0][i] = item_0;
      }

      // do the same for the second half of the proportion
      if (is_var_cpp(item_1) & (stack1.find(item_1) == stack1.end())) {
	res[1][i] = item_1 + "," + std::to_string(item_1_frq) + ">";
	stack1.insert(item_1);
      } else if ((item_1 != "-") & !is_var_cpp(item_1)){
	res[1][i] = item_1;
      }
    }

    // the traverse the proportion and remove enpty aligned cells
    for (int i = (res[0].size() - 1); i > -1; i --) {
      if ((res[0][i] == "") & (res[1][i] == "")) {
	res[0].erase(res[0].begin() + i);
	res[1].erase(res[1].begin() + i);
      }
    }
      
    // search for metathesis
    if (metathesis > 0) {
      std::vector<std::vector<std::string>> meta =
	find_metathesis_cpp(res, metathesis);

      std::string meta_s = analogy_flatten_cpp(meta);
      std::string res_s = analogy_flatten_cpp(res);
      all_analogies.push_back(meta_s);
      all_analogies.push_back(res_s);
    } else {
      std::string res_s = analogy_flatten_cpp(res);
      all_analogies.push_back(res_s);
    }
      
  }
  
  std::sort(all_analogies.begin(), all_analogies.end());
  all_analogies.erase(unique(all_analogies.begin(), all_analogies.end()), all_analogies.end());
  return all_analogies;
}

//' analogy_build_multiple_cpp
//' 
//' Returns all proportional analogies between all `str1` and `str2`.
//'
//' @param str1 Reference string (as a vector of vectors of characters)
//' @param str2 Target string (as a vector of vectors of characters)
//' @param metathesis Maximum number of characters which might be involved in a metathesis alternation. If set to 0, no metathesis is considered.
//' 
//' @return A list of analogy pairs.
//' 
//' @examples
//' analogy_build_cpp(c(c("a", "r", "b", "o"), c("a", "r", "b", "i"))
//'                 , (c("a", "r", "b", "a"), c("a", "r", "b", "o"))
//'                 , 0)
// [[Rcpp::export]]
std::vector<std::vector<std::string>>
analogy_build_multiple_cpp(const std::vector<std::vector<std::string>> str1,
			   const std::vector<std::vector<std::string>> str2,
			   int metathesis = 5) {
  std::vector<std::vector<std::string>> res;
  res.reserve(str1.size() * 40);
  for (int i = 0; i < str1.size(); i++) {
    std::vector<std::string> an = analogy_build_cpp(str1[i], str2[i], metathesis);
    res.push_back(an);
  }
  
  return res;
}

//' analogy_build_parallel_cpp
//' 
//' Parallel version of analogy_build_multiple_cpp.
//' Returns all proportional analogies between `str1` and `str2`.
//'
//' @param str1 Reference strings (as a vector of vectors characters)
//' @param str2 Target strings (as a vector of vectors of characters)
//' @param metathesis Maximum number of characters which might be involved in a metathesis alternation. If set to 0, no metathesis is considered.
//' 
//' @return A list of analogy pairs.
//' 
//' @examples
//' analogy_build_parallel_cpp(c(c("a", "r", "b", "o"), c("a", "r", "b", "i"))
//'                          , c(c("a", "r", "b", "a"), c("a", "r", "b", "a"))
//'                          , 0)
// [[Rcpp::export]]
std::vector<std::vector<std::string>>
analogy_build_parallel_cpp(const std::vector<std::vector<std::string>> str1,
			   const std::vector<std::vector<std::string>> str2,
			   int metathesis = 5) {
  
  // allocate the matrix we will return
  int reps = str1.size();
  std::vector<std::vector<std::string>> all_analogies (reps);

  tbb::parallel_for( 0, reps, 1, [&all_analogies, &str1, &str2, &metathesis](int i){

    std::vector<std::string> an1 = analogy_build_cpp(str1[i], str2[i], metathesis);
    all_analogies[i] = an1;

  }) ;

  return all_analogies;
}
