#ifndef ALIGN_UTIL_H
#define ALIGN_UTIL_H

#include <Rcpp.h>

// [[Rcpp::plugins(cpp11)]]

using namespace Rcpp;

std::vector<std::string> get_alignments_cpp(CharacterMatrix m, std::vector<std::string> op);

bool is_var_cpp (const std::string& el);

int find_var_id_cpp(const std::string& s);

int find_var_rep_cpp(const std::string& s);

bool is_analogy_cpp(const std::vector<std::string>& analogy);

std::string analogy_flatten_cpp(std::vector<std::vector<std::string>>& analogy);

std::string string_flatten_cpp(const std::vector<std::string>& str);

#endif
