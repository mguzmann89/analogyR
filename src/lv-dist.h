#ifndef LV_DIST_H
#define LV_DIST_H

#include <Rcpp.h>

// [[Rcpp::plugins(cpp11)]]

using namespace Rcpp;

std::vector<std::vector<double>>
lv_cpp_m(const std::vector<std::string> str1,
	 const std::vector<std::string> str2,
	 std::unordered_map<std::string, int> phon_map,
	 const std::vector<double> M,
	 const std::string method);

double lv_cpp(const std::vector<std::string> str1,
	      const std::vector<std::string> str2,
	      std::unordered_map<std::string, int> phon_map,
	      const std::vector<double> M,
	      const std::string method);

NumericMatrix
lv_parallel(const std::vector<std::vector<std::string>> mat,
	    const std::vector<std::string> cnames,
	    const std::vector<double> M,
	    const std::string method);

#endif
