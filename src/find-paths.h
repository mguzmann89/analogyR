#ifndef FIND_PATH_H
#define FIND_PATH_H

#include <Rcpp.h>

// [[Rcpp::plugins(cpp11)]]

using namespace Rcpp;

std::vector<std::string> sort_path_cpp (const std::vector<std::string> path);

std::vector<std::vector<std::string>> find_paths_cpp(const std::vector<std::string> str1,
						     const std::vector<std::string> str2);


#endif
