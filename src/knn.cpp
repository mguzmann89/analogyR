#include <Rcpp.h>
#include <RcppParallel.h>

using namespace RcppParallel;
using namespace Rcpp;

// [[Rcpp::depends(RcppParallel)]]

//' knn_row_cpp
//'
//' find nearest neighbors of row
//'
//' @param str1 String to align to (as vector of characters).
//' @param str2 String to align from (as vector of characters).
//' @return The Levenshtein distance between `str1` and `str2`.
//' @examples
//' str_dist_cpp(c("a", "b", "a"), c("a", "b", "o"))
// [[Rcpp::export]]
std::vector<int> knn_row_cpp(const std::vector<double> row,
                             const std::vector<int> classes, const int k = 5) {
  if (classes.size() != row.size())
    stop("Error: number of classes does not match the width of the matrix");
  if (k > classes.size())
    stop("Error: k cannot be larger than the number of classes");
  std::vector<int> idx(classes.size());
  std::iota(idx.begin(), idx.end(), 0);
  std::sort(idx.begin(), idx.end(),
            [&](int A, int B) -> bool { return row[A] < row[B]; });

  std::vector<int> res(k);
  for (int i = 0; i < k; i++) {
    res[i] = classes[idx[i]];
  }
  return res;
}

// make things parallel
struct KNN : public Worker {
  // input matrix to read from
  const NumericMatrix D;
  const std::vector<int> classes;
  const int k;
  // std::vector<std::vector<std::string>> rmat;  // output matrix to write to
  RMatrix<int> rmat; // output matrix to write to

  KNN(const NumericMatrix D, const std::vector<int> classes, const int k,
      IntegerMatrix rmat)
      : D(D), classes(classes), k(k), rmat(rmat) {}

  void operator()(std::size_t begin, std::size_t end) {
    for (std::size_t i = begin; i < end; i++) {
      std::vector<double> row_i(D.ncol());
      for (int j = 0; j < row_i.size(); j++) {
        row_i[j] = D(i, j);
      }

      std::vector<int> knns = knn_row_cpp(row_i, classes, k);
      for (int j = 0; j < k; j++) {
        rmat(i, j) = knns[j];
      }
    }
  }
};

//' knn_parallel
//'
//' Generate parallel distance matrix for all members of `mat`
//'
//' @param mat Distance matrix
//' @param classes Classes
//' @param k Number of neighbors
//' @return A matrix with the classes of the nearest neighbors for each element in mat
//' @examples knn_parallel(D, classes, 5)
// [[Rcpp::export]]
IntegerMatrix knn_parallel(const NumericMatrix D,
                           const std::vector<int> classes, const int k) {
  // allocate the matrix we will return
  // std::vector<std::vector<std::string>> rmat(classes.size(),
  // std::vector<std::string>(k));
  IntegerMatrix rmat(classes.size(), k);
  // create the worker
  KNN knnParallel(D, classes, k, rmat);
  // call it with parallelFor
  parallelFor(0, classes.size(), knnParallel);
  return rmat;
}
