#include <Rcpp.h>
#include "lv-dist.h"
#include "utils.h"
#include "get-alignments.h"
#include "get-backtrace.h"

using namespace Rcpp;

// [[Rcpp::plugins(cpp11)]]

//' sort_path_cpp
//' 
//' Sort an alignment path so that "dlul" -> "dllu".
//' This is useful to avoid duplication of equal alignments
//'
//' @param path Path to sort.
//' 
//' @return A sorted path.
//' 
//' @examples
//' sort_path_cpp(c("d", "l", "u", "l"))
//' 
// [[Rcpp::export]]
std::vector<std::string> sort_path_cpp (const std::vector<std::string> path){
  // sort paths 'ul' -> 'lu', leav 'd' in place
  std::vector<std::string> res;
  res.reserve(path.size());
  std::vector<std::string> subpath;
  subpath.reserve(path.size()*2);

  bool flag = false;

  for (int i = 0; i < path.size(); i++ ) {
    if (path[i] == "d") {
      if (flag) {
	std::sort(subpath.begin(), subpath.end());
	res.insert(res.end(), subpath.begin(), subpath.end());
	subpath.clear();
      }
      flag = false;
      res.push_back("d");
    } else {
      flag = true;
      subpath.push_back(path[i]);
    }
  }
  std::sort(subpath.begin(), subpath.end());
  res.insert(res.end(), subpath.begin(), subpath.end());
  
  return res;
}

//' find_paths_cpp
//' 
//' Find all alignment paths between str1 and str2.
//'
//' @param str1 Reference string
//' @param str2 Target string
//' 
//' @return A list of sorted paths.
//' 
//' @examples
//' find_paths_cpp(c("h", "o", "l", "a")
//'              , c("h", "a", "l", "i"))
//' 
// [[Rcpp::export]]
std::vector<std::vector<std::string>>
find_paths_cpp(const std::vector<std::string> str1,
	       const std::vector<std::string> str2) {

  auto als = get_alignments_cpp(get_backtrace2_cpp(str1, str2));
  int n_paths = std::count(als.begin(), als.end(), "#");

  std::vector<std::vector<std::string>> res_paths (n_paths);
  std::vector<std::string> path;
  path.reserve(str1.size()*2);

  int j = 0;
  for (int i = 0; i < als.size(); i ++ ) {
    if (als[i] == "#") {
      // std::vector<std::string> path_sorted = sort_path_cpp(path);
      res_paths[j] = sort_path_cpp(path);
      j++;
      path.clear();
    } else {
      path.push_back(als[i]);
    }    
  }

  std::sort(res_paths.begin(), res_paths.end());
  res_paths.erase(unique(res_paths.begin(), res_paths.end()), res_paths.end());

  // count number of 'd's

  int res_size = res_paths.size();
  
  std::vector<int> d_size (res_size);
  int max_d = 0;
  for (int i = 0; i < res_size; i++ ) {
    int d_i = std::count(res_paths[i].begin(), res_paths[i].end(), "d");
    d_size[i] = d_i;
    if (d_i > max_d)
      max_d = d_i;
  }

  // remove paths with too few 'd's
  for (int i = res_size-1; i >= 0; i--) {
    if (d_size[i] < max_d)
      res_paths.erase(res_paths.begin() + i);
  }
  
  return res_paths;
}
