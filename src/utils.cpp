#include <Rcpp.h>
#include "utils.h"

using namespace Rcpp;
// [[Rcpp::plugins(cpp11)]]

std::vector<std::string> append_vecs(const std::vector<std::string> v1,
				     const std::vector<std::string> v2){
  int v1l = v1.size();
  int v2l = v2.size();
  int nvl = v1l+v2l;
  std::vector<std::string> nv(nvl);
  for(int j = 0; j<nvl; j++){
    if(j<v1l)
      nv[j] = v1[j];
    else
      nv[j] = v2[j-v1l];
  }
  return nv;
}

std::vector<std::string> append_vecs2(std::vector<std::string> v1,
				      std::vector<std::string> v2,
				      std::vector<std::string> v3){
  auto nv = append_vecs(append_vecs(v1, v2), v3);
  return nv;
}
