#include <Rcpp.h>
#include "align.h"

using namespace Rcpp;

// [[Rcpp::plugins(cpp11)]]

// //' search_meta_cpp
// //' 
// //' Return the positions of the metathesis characters in `v2`.
// //' `v1` must start with the first metathesis character.
// //'
// //' @param v1 reference vector
// //' @param v2 target vector
// //' @param mx maximum number of positions to look at
// //' @return A vector with the alternation (unsimplified).
// //' 
// //' @examples
// //' search_meta_cpp(c("a", "r", "b", "" , "" , "l", "", "x", "t")
// //'               , c("" , "" , "b", "a", "r", "l", "t", "x", "")
// //'               , 5)
// //' # => c(3, 4)
// // [[Rcpp::export]]
// std::vector<int> search_meta_cpp(const std::vector<std::string> v1,
// 				 const std::vector<std::string> v2,
// 				 const int init,
// 				 const int mx) {
//   int vsize = v1.size();
//   int mx2 = mx + init;
//   if ((vsize - init) < 2) return std::vector<int> {-1};
//   std::vector<std::string> x;
//   for (int i = init; i < std::min(mx2, vsize); i++) {
//     if(v1[i] != v2[i]) {
//       x.push_back(v1[i]);
//     } else {
//       break;
//     }
//   }

//   // find the starting position of x in v2
//   if ((x.size() == 0) ) return std::vector<int> {-2};
//   if (((x.size() + init) >= vsize) ) return std::vector<int> {-5};

//   int j = -1;
//   for (int i = x.size() + init; i < std::min(mx2, vsize); i++) {
//     if(v1[i] != v2[i]) {
//       j = i;
//       break;
//     }            
//   }
  
//   if (((x.size() + init) >= vsize) | (j == -1))
//     return std::vector<int> {-3};
  
//   // if all worked out, check x is in v2
//   for(int i = 0; i < x.size(); i ++){
//     if ((j+i) >= vsize) {
//       return std::vector<int> {-4};
//     }
//     else {
//       if (!(v2[(j+i)] == x[i])) {
// 	return std::vector<int> {-4};
//       }
//     }
//   }
//   // build results
//   std::vector<int> res(x.size());
//   std::iota(res.begin(), res.end(), j);
//   return res;
// }

//' make_proportion_cpp
//' 
//' Return alternation from string and alignment
//'
//' @param vs string as vector of characters
//' @param al alignment as vector of characters
//' @export
//' @return A vector with the alternation (unsimplified).
// [[Rcpp::export]]
std::vector<std::vector<std::string> >
make_proportion_cpp(const std::vector<std::string> str1,
		    const std::vector<std::string> str2,
		     const std::vector<std::string> path,
		     const int metathesis = 0){
  std::vector<std::vector<std::string> > alignement = align_cpp(str1, str2, path);
  std::vector<std::vector<std::string> > res
    (2, std::vector<std::string>(alignement[0].size(), "-"));

  int j = 1;
  bool S = false;
  for (int i = 0; i < alignement[0].size(); i++) {
    if ((alignement[0][i] == alignement[1][i])) { // only change if not already mutated
      res[0][i] = "<" + std::to_string(j);
      res[1][i] = "<" + std::to_string(j);
      S = true;
    } else {
      if (res[0][i] == "-") res[0][i] = alignement[0][i];
      if (res[1][i] == "-") res[1][i] = alignement[1][i];
      if(S) {
	j++;
	S = false;
      }
    } 
  }
  return res;
}

// //' build_analogy_cpp
// //' 
// //' Return the positions of the metathesis characters in `v2`.
// //' `v1` must start with the first metathesis character.
// //'
// //' @param v1 reference vector
// //' @param v2 target vector
// //' @param mx maximum number of positions to look at
// //' @return A vector with the alternation (unsimplified).
// //' 
// //' @examples
// //' build_analogy_cpp(c("a", "r", "b", "" , "" , "l", "", "x", "t")
// //'               , c("" , "" , "b", "a", "r", "l", "t", "x", "")
// //'               , 5)
// //' # => c(3, 4)
// // [[Rcpp::export]]
// std::vector<std::vector<std::string> > build_analogy_cpp(const std::vector<std::string> str1,
// 							 const std::vector<std::string> str2,
// 							 const std::vector<std::string> path,
// 							 const int metathesis) {
//   std::vector<std::vector<std::string> > proportion =
//     make_proportion_cpp(str1, str2, path, metathesis);

//   std::unordered_set<std::string> stack0;
//   std::unordered_set<std::string> stack1;
//   std::map<std::string, int> var_counts;

//   std::vector<std::vector<std::string>>
//     res (2,std::vector<std::string> (proportion[0].size()));

//   for (int i = 0; i < proportion[0].size(); i++) {
//     if (proportion[0][i][0] == '<')
//       var_counts[proportion[0][i]]++;
//   }
  
//   std::string item_0 = "";
//   std::string item_1 = "";
//   int item_0_frq = 0;
//   int item_1_frq = 0;
  
//   for (int i = 0; i < proportion[0].size(); i++ ) {
//     item_0 = proportion[0][i];
//     item_1 = proportion[1][i];

//     item_0_frq = var_counts[item_0];
//     item_1_frq = var_counts[item_1];

//     if ((item_0[0] == '<') & (stack0.find(item_0) == stack0.end())) {
//       res[0][i] = item_0 + "," + std::to_string(item_0_frq) + ">";
//       stack0.insert(item_0);
//     } else if ((item_0 != "-") & (item_0[0] != '<')){
//       res[0][i] = item_0;
//     }

//     if ((item_1[0] == '<') & (stack1.find(item_1) == stack1.end())) {
//       res[1][i] = item_1 + "," + std::to_string(item_1_frq) + ">";
//       stack1.insert(item_1);
//     } else if ((item_1 != "-") & (item_1[0] != '<')){
//       res[1][i] = item_1;
//     }
//   }

//   return res;
// }

// //' build_analogy_cpp
// //' 
// //' Return the positions of the metathesis characters in `v2`.
// //' `v1` must start with the first metathesis character.
// //'
// //' @param v1 reference vector
// //' @param v2 target vector
// //' @param mx maximum number of positions to look at
// //' @return A vector with the alternation (unsimplified).
// //' 
// //' @examples
// //' build_analogy_cpp(c("a", "r", "b", "" , "" , "l", "", "x", "t")
// //'               , c("" , "" , "b", "a", "r", "l", "t", "x", "")
// //'               , 5)
// //' # => c(3, 4)
// // [[Rcpp::export]]
// std::vector<std::vector<std::string> > build_analogy_cpp(const std::vector<std::string> str1,
// 							 const std::vector<std::string> str2,
// 							 const std::vector<std::string> path,
// 							 const int metathesis) {
//   std::vector<std::vector<std::string> > proportion =
//     make_proportion_cpp(str1, str2, path, metathesis);

//   std::unordered_set<std::string> stack0;
//   std::unordered_set<std::string> stack1;
//   std::map<std::string, int> var_counts;

//   std::vector<std::vector<std::string>>
//     res (2,std::vector<std::string> (proportion[0].size()));
  
//   for (int i = 0; i < proportion[0].size(); i++) {
//     if (proportion[0][i].back() == '>')
//       var_counts[proportion[0][i]]++;
//   }
  
//   std::string item_0 = "";
//   std::string item_1 = "";
//   int item_0_frq = 0;
//   int item_1_frq = 0;
  
//   for (int i = 0; i < proportion[0].size(); i++ ) {
//     item_0 = proportion[0][i];
//     item_1 = proportion[1][i];

//     item_0_frq = var_counts[item_0];
//     item_1_frq = var_counts[item_1];

//     if (item_0.back() == '>') {
//       if (item_0_frq == 1) {// variable with 1
// 	res[0][i] = "<Y" + item_0;
//       }
//       else if ((item_0_frq == 2) & (stack0.find(item_0) == stack0.end())) { 
// 	stack0.insert(item_0);
// 	res[0][i] = "<Z" + item_0;
//       }
//       else if ((item_0_frq > 2) & (stack0.find(item_0) == stack0.end())) { 
// 	stack0.insert(item_0);
// 	res[0][i] = "<X" + item_0;
//       }
//     } else if (item_0 != "-"){
//       res[0][i] = item_0;
//     }
    
//     if (item_1.back() == '>') {
//       if (item_1_frq == 1) {// variable with 1
// 	res[1][i] = "<Y" + item_1;
//       }
//       else if ((item_1_frq == 2) & (stack1.find(item_1) == stack1.end())) { 
// 	stack1.insert(item_1);
// 	res[1][i] = "<Z" + item_1;
//       }
//       else if ((item_1_frq > 2) & (stack1.find(item_1) == stack1.end())) {
// 	stack1.insert(item_1);
// 	res[1][i] = "<X" + item_1;
//       }
//     } else if (item_1 != "-"){
//       res[1][i] = item_1;
//     }
//   }

//   return res;
// }
