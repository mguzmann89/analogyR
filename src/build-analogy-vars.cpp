#include <Rcpp.h>
#include <unordered_set>

using namespace Rcpp;

// [[Rcpp::plugins(cpp11)]]

//' build_analogy_vars_cpp
//' 
//' From an analogy in form of a list of pairs: list(c("X", "1"), c("X", "1"), c("ab"), ...) it constructs analogy variables such that the first instance of X1 will be (?P<X1>.+) and the following instances will be (?P=X1). This is necessary fro group matching.
//'
//' @param str List of analogy pieces
//' @export
//' @return A vector with the analogy vars constructed.
// [[Rcpp::export]]
std::vector<std::string> build_analogy_vars_cpp(std::vector<std::vector<std::string> > str) {
  std::unordered_set<std::string> stack; // keep track of seen ids
  std::vector<std::string> res(str.size());
  for (int i = 0; i < str.size(); i++) {
    if (str[i].size() == 2) {
      std::string vr = str[i][0];
      std::string reps = str[i][1];
      if (stack.find(vr) == stack.end()) {
	if (reps == "+")
	  res[i] = "(?P<"+vr+">.+)";
	else
	  res[i] = "(?P<"+vr+">.{" + reps + "})";
      } else {
	res[i] = "(?P="+vr+")";
      }
      stack.insert(vr);
    } else {
      res[i] = str[i][0];
    }      
  }
  return res;
}
