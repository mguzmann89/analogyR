#include <Rcpp.h>
#include "utils.h"
#include "lv-dist.h"

using namespace Rcpp;

// [[Rcpp::plugins(cpp11)]]

//' get_alignments_cpp
//' 
//' From a backtrace matrix, return all possible paths which solve the alignment
//'
//' This function only searchers for paths without insertion
//' This is because we assume the stem is fully contained in the form,
//' which means there are no need for insertions.
//' 
//' @param m Character Matrix with the backtrace.
//' @param op A string to use for separating the alignments.
//' @export
//' @return A vector with all alignments. The alignments are separated by op.
// [[Rcpp::export]]
std::vector<std::string> get_alignments_cpp(std::vector<std::vector<std::string>> m){

  std::vector<std::string> l = {"l"};
  std::vector<std::string> d = {"d"};
  std::vector<std::string> u = {"u"};
  
  std::function<std::vector<std::string>(int, int, std::vector<std::string>)>
    get_alignments_rec = [&] (int nr, int nc, std::vector<std::string> op) {
      if((nr==1) & (nc==1)) // base condition
	return op;
      else{ // otherwise do a recursive call
	if (m[nr-1][nc-1] == "u"){
	  return get_alignments_rec(nr-1, nc, append_vecs(u, op));
	}
	if (m[nr-1][nc-1]=="lu"){
	  return append_vecs(get_alignments_rec(nr, nc-1, append_vecs(l, op)),
			     get_alignments_rec(nr-1, nc, append_vecs(u, op)));
	}
	if (m[nr-1][nc-1]=="ud"){
	  return append_vecs(get_alignments_rec(nr-1, nc-1, append_vecs(d, op)),
			     get_alignments_rec(nr-1, nc, append_vecs(u, op)));
	}
	if (m[nr-1][nc-1]=="d"){ // d
	  return get_alignments_rec(nr-1, nc-1, append_vecs(d, op));// move diagonally
	}
	if (m[nr-1][nc-1]=="l"){ // l
	  return get_alignments_rec(nr, nc-1, append_vecs(l, op)); // move left
	}
	if (m[nr-1][nc-1]=="ld") // l and d
	  return append_vecs(get_alignments_rec(nr-1, nc-1, append_vecs(d, op)), // d
			     get_alignments_rec(nr, nc-1, append_vecs(l, op))); // l
	if (m[nr-1][nc-1]=="lud") // l and d
	  return append_vecs2(get_alignments_rec(nr-1, nc-1, append_vecs(d, op)), //d
			      get_alignments_rec(nr, nc-1, append_vecs(l, op)), // l
			      get_alignments_rec(nr-1, nc, append_vecs(u, op))); // u
      }
    };  
  return get_alignments_rec(m.size(), m[0].size(), {"#"});

}
