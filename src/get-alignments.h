#ifndef GET_ALIGN_H
#define GET_ALIGN_H

#include <Rcpp.h>

// [[Rcpp::plugins(cpp11)]]

using namespace Rcpp;

std::vector<std::string> get_alignments_cpp(std::vector<std::vector<std::string>> m);

#endif
