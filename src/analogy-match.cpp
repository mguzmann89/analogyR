#include <RcppParallel.h>
#include <Rcpp.h>
#include "analogy-utils.h"

using namespace Rcpp;
using namespace RcppParallel;

// [[Rcpp::depends(RcppParallel)]]
// [[Rcpp::plugins(cpp11)]]

//' analogy_match_cpp
//' 
//' Check whether the left hand side of an analogy can be matched
//' against a string.
//'
//' @param str String to match against
//' @param analogy Full analogy to match
//' 
//' @return TRUE or FALSE
//'
//' @export
//' 
//' @examples
//' 
//' analogy_match_cpp(c("a", "b", "a"), c("<X1,1>", "b", "a", "⇋", "<X1,1>))      # => TRUE
//' analogy_match_cpp(c("a", "d", "b", "a"), c("<X1,1>", "b", "a", "⇋", "<X1,1>)) # => TRUE
//' 
// [[Rcpp::export]]
bool analogy_match_cpp(const std::vector<std::string>& str,
		       const std::vector<std::string>& analogy) {

  if (!is_analogy_cpp(analogy)) {
    Rcout << "Not a valid analogy.";
    return(false);
  }
  // first find all reps for variables
  // and the total number

  std::vector<std::string> analogy_left;
  analogy_left.reserve(analogy.size());

  for (int i = 0; i < analogy.size(); i++) {
    if (analogy[i] == "⇋")
      break;
    else {
      analogy_left.push_back(analogy[i]);
    }
  }

  // if the string fewer elements than the left hand side,
  // the analogy cannot match
  if (str.size() < analogy_left.size()) {
    return false;
  }
  if (str == analogy_left) {
    return true;
  }
  
  int total_match = 0;
  int var_lengths = 0;
  std::map<int, int> var_reps;
  std::map<int, std::vector<std::string>> var_matches;
  
  // calculate the max number of matching characters for the free variable
  for (int i = 0; i < analogy_left.size(); i++) {

    if (is_var_cpp(analogy_left[i])) {
      int rep = find_var_rep_cpp(analogy_left[i]);
      int id = find_var_id_cpp(analogy_left[i]);
      var_reps[id] = rep;
      total_match = total_match + rep;
    } else {
      var_lengths++;
    }
    
  }

  // can't be matched because not enough items
  if ((total_match + var_lengths) >= str.size())
    return false;
  
  int zero_match = str.size() - total_match - var_lengths;

  int k = 0;
  for (int i = 0; i < analogy_left.size(); i++ ) {
    if (k >= str.size()) {
      return false;
    }
    if (is_var_cpp(analogy_left[i])) {
      int rep = find_var_rep_cpp(analogy_left[i]);
      int id = find_var_id_cpp(analogy_left[i]);
      std::vector<std::string> match;
      match.reserve(rep*2);
      
      if (rep == 0) rep = zero_match;
      
      for (int j = 0; j < rep; j++ ) {
	match.push_back(str[k]);
	k++;
      }

      // if key exists but match is not consistent
      // return str: <X1,2> <X1,2> ~ capr
      // X1 should match ca and pr but these are different
      if (var_matches.find(id) == var_matches.end() ) {
	var_matches[id] = match;
      } else {
	if (!(match == var_matches[id])) {
	  return false;
	}
      }

    } else {
      if (analogy_left[i] != str[k])
	return false;
      k++;
    }
  }
  return true;
}

// just to handle overabundance
// every analogy can be a list of analogies
// if anyone matches, then true
// [[Rcpp::export]]
bool analogy_match2_cpp(const std::vector<std::string>& str,
			const std::vector<std::vector<std::string>>& analogy) {

  for (int i = 0; i < analogy.size(); i++){
    bool res_t = analogy_match_cpp(str, analogy[i]);
    if (res_t)
      return true;
  }
  return false;
}

//' analogy_match_parallel_cpp
//' 
//' Parallel version of analogy_match_cpp
//'
//' @param str List of strings to match against
//' @param analogy List of analogies to match
//' @param nest Indicates whether to do paired matching, or to nest along the analogies ("an")
//' or along the strings ("str")
//' If "paired" (the default), both `str` and `analogy` must have the same length.
//' 
//' @return TRUE or FALSE
//'
//' @export
//' 
//' @examples
//' 
//' analogy_match_cpp(list(c("a", "b", "a"), c("a", "d", "b", "a"))
//'                      , c("<X1,1>", "b", "a", "⇋", "<X1,1>")
//'                      , nest = "an")
//' 
//' 
// [[Rcpp::export]]
std::vector<std::vector<bool>>
analogy_match_parallel_cpp(const std::vector<std::vector<std::string>> str,
			   const std::vector<std::vector<std::vector<std::string>>> analogy,
			   const std::string nest = "paired") {
  
  // allocate the matrix we will return
  std::vector<std::vector<bool>> res;
  
  if (nest == "paired") {

    // check if sizes match
    if (str.size() != analogy.size())
      stop("The list of strings and analogies must have the same size for nested matching.");
    
    int reps = str.size();
    res.resize(reps);

    tbb::parallel_for( 0, reps, 1, [&res, &str, &analogy](int i){

      bool an_matches = analogy_match2_cpp(str[i], analogy[i]);
      res[i] = {an_matches};

    });
  } else if (nest == "str") {
    int reps = str.size();
    res.resize(reps);

    tbb::parallel_for( 0, reps, 1, [&res, &str, &analogy](int i){

      std::vector<bool> an_matches (analogy.size());
      
      for (int j = 0; j < analogy.size(); j++) {
	an_matches[j] = analogy_match2_cpp(str[i], analogy[j]);
      }
      
      res[i] = an_matches;
    });
  } else if (nest == "an") {
    int reps = analogy.size();
    res.resize(reps);

    tbb::parallel_for( 0, reps, 1, [&res, &str, &analogy](int i){

      std::vector<bool> an_matches (str.size());
      
      for (int j = 0; j < str.size(); j++) {
	an_matches[j] = analogy_match2_cpp(str[j], analogy[i]);
      }
      
      res[i] = an_matches;

    });
  } else {
    stop("`nest` has to be either 'paired', 'an' or 'str'.");
  }

  return res;
}

// [[Rcpp::export]]
std::vector<std::vector<bool>>
analogy_match_knn_cpp(const std::vector<std::vector<std::string>> str,
		      const std::vector<std::vector<std::vector<std::vector<std::string>>>> analogy) {

  int reps = str.size();
  std::vector<std::vector<bool>> res (reps);
  
  tbb::parallel_for( 0, reps, 1, [&res, &str, &analogy](int i){
    std::vector<bool> res_tmp (analogy[i].size());
    bool res_tmp_j = false;

    for (int j = 0; j< analogy[i].size(); j++) {
      res_tmp_j = analogy_match2_cpp(str[i], analogy[i][j]);
      res_tmp[j] = res_tmp_j;
    }
    
    res[i] = res_tmp;
  });
  
  return res;  
}
