#include <Rcpp.h>

using namespace Rcpp;

// [[Rcpp::plugins(cpp11)]]

// [[Rcpp::export]]
bool is_var_cpp (const std::string& el) {
  return (el[0] == '<');  
}

// find the id of a variable
// [[Rcpp::export]]
int find_var_id_cpp(const std::string& s) {
  if (is_var_cpp(s)) {
    std::string::size_type p  = s.find('X');
    std::string::size_type pp = s.find(',', p + 2); 
    return std::stoi(s.substr(p + 1, pp - p - 1));
  }
  else return -1;
}

// find the number of matching substrings of a variable
// [[Rcpp::export]]
int find_var_rep_cpp(const std::string& s) {
  if (is_var_cpp(s)) {
    std::string::size_type p  = s.find(',');
    std::string::size_type pp = s.find('>', p + 2); 
    return std::stoi(s.substr(p + 1, pp - p - 1));
  }
  else return -1;
}

// [[Rcpp::export]]
bool is_analogy_cpp(const std::vector<std::string>& analogy) {
  if (std::find(analogy.begin(), analogy.end(), "⇋") == analogy.end()) {
    return false;
  } else
    return true;
}

// flatten an analogy (2d vector) into a string
// [[Rcpp::export]]
std::string analogy_flatten_cpp(std::vector<std::vector<std::string>>& analogy) {

  std::vector<std::string> an (analogy.size()*2 + 2);
  an.insert(an.end(), analogy[0].begin(), analogy[0].end());
  an.push_back("⇋");
  an.insert(an.end(), analogy[1].begin(), analogy[1].end());

  an.erase(std::remove(an.begin(), an.end(), ""), an.end());

  std::stringstream res;
  for (int i = 0; i < (an.size()-1); i ++ ){
    res << an[i] << " ";
  }
  res << an.back();
  
  return res.str();
}

// flatten a string vector into a string
// [[Rcpp::export]]
std::string string_flatten_cpp(const std::vector<std::string>& str) {
  std::stringstream res;
  for (int i = 0; i < str.size(); i ++ ){
    res << str[i] << "";
  }  
  return res.str();
}
