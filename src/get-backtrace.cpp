#include <Rcpp.h>
#include "utils.h"
#include "lv-dist.h"

using namespace Rcpp;

// [[Rcpp::plugins(cpp11)]]

//' get_backtrace_cpp
//' 
//' From a levenshtein matrix get the backtrace
//'
//' The backtrace is a matrix with the operations needed to go from `str2` to `str1`
//' @param str1 String to align to
//' @param str2 String to align from
//' @export
//' @return A character matrix
// [[Rcpp::export]]
std::vector<std::vector<std::string>>
get_backtrace_cpp(const std::vector<std::string> str1,
		  const std::vector<std::string> str2){

  std::vector<std::vector<double>> m =
    lv_cpp_m(str1, str2,
	     std::unordered_map<std::string, int>(),
	     std::vector<double>(),
	     "lv");
  
  int nr = m.size();
  int nc = m[0].size();
  std::vector<std::vector<std::string>> dm(nr, std::vector<std::string> (nc)); // return matrix
  // populate first row and column

  for (int c = 1; c<nc; c++)
    dm[0][c] = "l";
  for (int r = 1; r<nr; r++)
    dm[r][0] = "u";
  
  dm[0][0] = "n";
  // populate the rest
  for (int c = nc-1; c>=1; c--){
    for (int r = nr-1; r>=1; r--){
      int minval = std::min(std::min(m[r][c-1], m[r-1][c]), m[r-1][c-1]);
      std::string step;
      if (m[r][c-1] <= m[r][c])
	step = "l";
      if (m[r-1][c] <= m[r][c])
	step += "u";
      if ((str1.at(r-1) == str2.at(c-1)) && // check if we can go diagonally
	  (m[r-1][c-1] == m[r][c])) 
	step += "d";

      //check diag only
      if ((minval == m[r][c]) & (m[r][c] == m[r-1][c-1])) {
	step = "d";
      }
      
      dm[r][c] = step;
      dm;
    }
  }
  return dm;
}

//' get_backtrace2_cpp
//' 
//' From a levenshtein matrix get the backtrace
//'
//' The backtrace is a matrix with the operations needed to go from `str2` to `str1`
//' @param str1 String to align to
//' @param str2 String to align from
//' @export
//' @return A character matrix
// [[Rcpp::export]]
std::vector<std::vector<std::string>>
get_backtrace2_cpp(const std::vector<std::string> str1,
		   const std::vector<std::string> str2){

  std::vector<std::vector<double>> m =
    lv_cpp_m(str1, str2,
	     std::unordered_map<std::string, int>(),
	     std::vector<double>(),
	     "lv");
  
  int nr = m.size();
  int nc = m[0].size();
  std::vector<std::vector<std::string>> dm(nr, std::vector<std::string> (nc)); // return matrix
  // populate first row and column

  for (int c = 1; c<nc; c++)
    dm[0][c] = "l";
  for (int r = 1; r<nr; r++)
    dm[r][0] = "u";
  
  dm[0][0] = "n";
  // populate the rest
  for (int c = nc-1; c>=1; c--){
    for (int r = nr-1; r>=1; r--){
      int minval = std::min(std::min(m[r][c-1], m[r-1][c]), m[r-1][c-1]);
      std::string step = "";

      if ((minval == m[r][c]) & (m[r][c] == m[r-1][c-1])) {
	step = "d";
      } else {
	if (m[r][c-1] == minval)
	  step += "l";
	if (m[r-1][c] == minval)
	  step += "u";
	if (m[r-1][c-1] == minval)
	  step += "d";
      }
      //check diag only
            
      dm[r][c] = step;
      dm;
    }
  }
  return dm;
}
