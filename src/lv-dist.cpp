#include <Rcpp.h>
#include <RcppParallel.h>
#include "lv-dist.h"
#include "utils.h"

using namespace RcppParallel;
using namespace Rcpp;

// [[Rcpp::depends(RcppParallel)]]

//' lv_cpp_m
//' 
//' Generate levenshtein matrix for str1 and str2
//'
//' @param str1 String to align to.
//' @param str2 String to align from.
//' @param phon_map Map with phoneme names and their index in M (as matrix). Empty vector results in all distances being 1.
//' @param M Matrix of phoneme distances as a 1d vector.
//' @param method Method to be used. Currently two methods are supported `lv` is the traditional Levenshtein distance, while `lve` is right-hand-side weighted Levenshtein distance. 
//' @return A numeric matrix with the substition scores.
//' @examples
//' str_dist_cpp(c("a", "b", "a"), c("a", "b", "o"))
std::vector<std::vector<double>>
lv_cpp_m(const std::vector<std::string> str1,
	 const std::vector<std::string> str2,
	 std::unordered_map<std::string, int> phon_map,
 	 const std::vector<double> M,
	 const std::string method = "lv"){
  int lenStr1=str1.size()+1;
  int lenStr2=str2.size()+1;
  int m_nrow = sqrt(M.size()); // to acces M

  std::vector<std::vector<double>> D(lenStr1, std::vector<double>(lenStr2)); // return matrix
  
  for(int i=0; i<lenStr1; i++) D[i][0] = (double)i;
  for(int j=0; j<lenStr2; j++) D[0][j] = (double)j;
  
  double  cost = 0.0;
  std::vector<double> tmp(3);
  for(int i=1; i<lenStr1; i++){
    for(int j=1; j<lenStr2; j++){
      if(str1[i-1]==str2[j-1]) {
	cost=0.0; // check if equal
      } else { // cost for substitution
	if(M.size() > 0) // if given dist matrix
	  cost = M[phon_map[str1[i-1]]*m_nrow+phon_map[str2[j-1]]];
	else cost = 1.0;
      }
      
      // which method for distances to use
      
      if (method == "lv") {
	tmp[0] = D[i-1][j  ] + 1.0;    // left
	tmp[1] = D[i  ][j-1] + 1.0;    // up
	tmp[2] = D[i-1][j-1] + cost; // diagonal
	D[i][j] = MIN3(tmp[0], tmp[1], tmp[2]);
      } else if (method == "lve") {
	tmp[0]  = D[i-1][j  ] + 1.0 /std::max(j, i); // left
	tmp[1]  = D[i  ][j-1] + 1.0 /std::max(j, i); // up
	tmp[2]  = D[i-1][j-1] + cost/std::max(j, i); // diagonal
	D[i][j] = MIN3(tmp[0], tmp[1], tmp[2]);
      }
    }
  }
  return D;
}

//' distance_cpp_m
//' 
//' Generate levenshtein distance matrix for str1 and str2
//'
//' @param str1 String to align to.
//' @param str2 String to align from.
//' @param cnames Vector with phoneme names. Empty vector results in all distances being 1. Must be align with M as a matrix.
//' @param M Matrix of phoneme distances as a 1d vector.
//' @param method Method to be used. Currently two methods are supported `lv` is the traditional Levenshtein distance, while `lve` is right-hand-side weighted Levenshtein distance.
//' @return A numeric matrix with the substition scores.
//' @examples
//' distance_cpp_m(c("a", "b", "a"), c("a", "b", "o"), ...)
// [[Rcpp::export]]
std::vector<std::vector<double>>
distance_cpp_m(const std::vector<std::string> str1,
	       const std::vector<std::string> str2,
	       std::vector<std::string> cnames,
	       const std::vector<double> M,
	       const std::string method = "lv"){

  std::unordered_map<std::string, int> phon_map;
  for (int i = 0; i < cnames.size(); i++) {
    phon_map[cnames[i]] = i;
  }

  std::vector<std::vector<double>> D = lv_cpp_m(str1, str2, phon_map, M, method);
  return D;
}

//' lv_cpp
//' 
//' Generate unweighted levenshtein distance for str1 and str2
//'
//' @param str1 String to align to (as vector of characters).
//' @param str2 String to align from (as vector of characters).
//' @param phon_map map with phoneme names and their index in M (as matrix). Empty vector results in all distances being 1.
//' @param M matrix of distances as a 1d vector.
//' @param method Method to be used. Currently two methods are supported `lv` is the traditional Levenshtein distance, while `lve` is right-hand-side weighted Levenshtein distance.
//' @return The Levenshtein distance between `str1` and `str2`.
//' @examples
//' lv_cpp(c("a", "b", "a"), c("a", "b", "o"), ...)
double lv_cpp(const std::vector<std::string> str1,
	      const std::vector<std::string> str2,
	      const std::unordered_map<std::string, int> phon_map,
	      const std::vector<double> M,
	      const std::string method = "lv"){

  std::vector<std::vector<double>> D = lv_cpp_m(str1, str2, phon_map, M, method);
    
  return D[str1.size()][str2.size()];
}

// make things parallel
struct LvDistance : public Worker {
  // input matrix to read from
  const std::vector<std::vector<std::string> > mat;
  const std::unordered_map<std::string, int> phon_map;
  const std::vector<double> M;
  const std::string method;
  
  // output matrix to write to
  RMatrix<double> rmat;
  // initialize from Rcpp input and output matrixes (the RMatrix class
  // can be automatically converted to from the Rcpp matrix type)
  LvDistance(const std::vector<std::vector<std::string> > mat,
	     const std::unordered_map<std::string, int> phon_map,
	     const std::vector<double> M,
	     const std::string method,
	     NumericMatrix rmat)
    : mat(mat), phon_map(phon_map), M(M), method(method), rmat(rmat) {}
  // function call operator that work for the specified range (begin/end)
  void operator()(std::size_t begin, std::size_t end) {
    for (std::size_t i = begin; i < end; i++) {
      for (std::size_t j = 0; j < i; j++) {
	rmat(i,j) = lv_cpp(mat[i], mat[j], phon_map, M, method);;
      }
    }
  }
};

//' lv_parallel
//' 
//' Generate distance matrix for all members of `mat` in parallel using all availablel cores
//'
//' @param mat List of strings (as vectors of characters) to measure disntance to.
//' @param cnames Vector with phoneme names. Empty vector results in all distances being 1. Must be align with M as a matrix.
//' @param M Substition matrix for phoneme distance.
//' @param method Method to be used. Currently two methods are supported `lv` is the traditional Levenshtein distance, while `lve` is right-hand-side weighted Levenshtein distance.
//' @return A numeric vector with distances between str and all members of strs.
//' @examples
//' lv_parallel(list(c("a", "b", "a"), c("a", "b", "o"), c("a", "b", "u")), c("a", "b", "e"), ...)
// [[Rcpp::export]]
NumericMatrix
lv_parallel(const std::vector<std::vector<std::string> > mat,
	    const std::vector<std::string> cnames,
	    const std::vector<double> M,
	    const std::string method = "lv") {  
  // allocate the matrix we will return
  NumericMatrix rmat(mat.size(), mat.size());
  // create the worker
  std::unordered_map<std::string, int> phon_map;
  for (int i = 0; i < cnames.size(); i++) {
    phon_map[cnames[i]] = i;
  }
  
  LvDistance lvDistance(mat, phon_map, M, method, rmat);
  // call it with parallelFor
  parallelFor(0, mat.size(), lvDistance);
  return rmat;
}
