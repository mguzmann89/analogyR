// #include <Rcpp.h>
#include <RcppParallel.h>
#include <RcppArmadillo.h>

// [[Rcpp::depends(RcppArmadillo)]]
// [[Rcpp::depends(RcppParallel)]]

using namespace RcppParallel;
using namespace Rcpp;

//' knnc_parallel
//' 
//' Find k-nearest neighbors for each row of `D`. This function only makes sense when called from knnc.
//'
//' @param D Distance matrix
//' @param k Number of neighbors
//' @return A matrix with the values of the nearest neighbors for row of D
//' @examples
//' knnc_parallel(D, 5)
// [[Rcpp::export]]
arma::mat
knnc_parallel(arma::mat D, int k) {
  // allocate the matrix we will return

  int reps =  D.n_rows;
  arma::mat rmat (reps, k);

  tbb::parallel_for( 0, reps, 1, [&rmat, &D, &k](int i){
    arma::rowvec row_i = D.row(i);
    std::sort(row_i.begin(), row_i.end());
    rmat.row(i) = row_i.subvec(0, k-1);
  });

  return rmat;
}
