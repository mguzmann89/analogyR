#include <Rcpp.h>
using namespace Rcpp;
// [[Rcpp::plugins(cpp11)]]

// [[Rcpp::export]]
Rcpp::NumericMatrix nns_cpp(Rcpp::NumericMatrix distance,
			    int k,
			    bool summarise = false,
			    bool decreasing = false) {
  int n = distance.nrow();
  int k2 = k;
  if (summarise) k2 = 1;
  Rcpp::NumericMatrix out(n, k2);
  for (int i = 0; i < n; i++) {
    Rcpp::NumericVector row = distance(i , _ );
    row = row[! is_na(row)];
    row.sort(decreasing);
    if (summarise)
      out(i, 0) = Rcpp::mean(row[Rcpp::Range(0, k-1)]);
    else
      out(i, _) = row[Rcpp::Range(0, k-1)];
  }
  return out;  
}
