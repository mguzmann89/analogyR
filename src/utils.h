#ifndef UTILS_H
#define UTILS_H

// [[Rcpp::plugins(cpp11)]]

#define MIN3(a, b, c) ((a) < (b) ? ((a) < (c) ? (a) : (c)) : ((b) < (c) ? (b) : (c)))

std::vector<std::string> append_vecs(const std::vector<std::string> v1,
				     const std::vector<std::string> v2);

std::vector<std::string> append_vecs2(std::vector<std::string> v1,
				      std::vector<std::string> v2,
				      std::vector<std::string> v3);

#endif
